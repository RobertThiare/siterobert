import Navbar from './components/Navbar';
import Intro from './components/Intro';
import Projets from './components/Projets';
import Contacts from './components/Contacts';
import './App.css';
import Footer from './components/Footer';
import { themeContext} from './Context';
import { useContext } from 'react';

function App() {

  const theme= useContext(themeContext);
    const darkMode = theme.state.darkMode;
  return (
    <div className="App" style={{
      background: darkMode? 'black': '',
      color: darkMode? 'white': ''
    }}>
      <Navbar />
      <Intro/>
      <Projets />
      <Contacts />
      <Footer />
    </div>
  );
}

export default App;
