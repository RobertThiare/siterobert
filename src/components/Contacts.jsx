import React, { useState } from 'react';
import './Contacts.css';
import { useRef } from 'react';
import emailjs from '@emailjs/browser';


export default function Contacts() {

    const form = useRef();

    const [done, setDone] = useState(false);

    const sendEmail = (e) => {
      e.preventDefault();
  
      emailjs.sendForm('service_j0c6cjg', 'template_6b65j67', form.current, 'T89qGB76nwx9Rr8q2')
        .then((result) => {
            console.log(result.text);
            setDone(true);
        }, (error) => {
            console.log(error.text);
        });
    };

  return (
    <div className='contact-form' id='Contacts'>
        <span>Laissez-moi un message</span>

        <div className='c-right'>
            <form ref={form} onSubmit ={sendEmail}>
                <input type="text" name='user_name' className='user' placeholder='Nom'/>
                <input type="email" name='user_email' className='user' placeholder='Email'/>
                <textarea name="message" className='user' placeholder='Écrivez votre message ici ...'/>
                <input type="submit" value='Envoyer' className='button'/>
                <span style={{color: 'gray'}}>
                    {done && "Votre message a bien été envoyé!"}
                </span>
            </form>
        </div>
        
    </div>
  )
}
