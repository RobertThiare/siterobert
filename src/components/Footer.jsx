import React from 'react';
import './Footer.css';
import basPage from '../ressources/footer.png';
import Insta from '@iconscout/react-unicons/icons/uil-instagram';
import Facebook from '@iconscout/react-unicons/icons/uil-facebook';
import GitHub from '@iconscout/react-unicons/icons/uil-github';
import LinkedIn from '@iconscout/react-unicons/icons/uil-linkedin';
import GitLab from '@iconscout/react-unicons/icons/uil-gitlab';


export default function Footer() {
  return (
    <div className='footer'>
        <img src={basPage} alt="fond d'écran de bas de page" style={{width:'100%'}}/>
        <div className='f-content'>
            <span>thiarerobert07@protonmail.com</span>
            <div className='f-icons'>
                <a href="https://www.instagram.com/thiarerobert/">
                  <Insta color='black' size='3rem' />
                </a>
                <a href="https://www.facebook.com/robert.thiare/">
                  <Facebook color='black' size='3rem' />
                </a>
                <a href="https://github.com/thiarerobert">
                  <GitHub color='black' size='3rem' />
                </a>
                <a href="www.linkedin.com/in/robert-thiare-1150b81a8">
                  <LinkedIn color='black' size ='3rem'/>
                </a>
                <a href="https://gitlab.com/RobertThiare">
                  <GitLab color='black' size ='2rem'/>
                </a>
            </div>
        </div>
          <div className='ressources'>
            <span>Ressources utilisées</span>
            <span>Pixabay : <a href="https://pixabay.com/">ici</a></span>
            <span>Microsoft Bing : <a href="https://www.bing.com/">ici</a></span>
          </div>
          <div className='copy'>
            <span>&copy; Robert Thiare </span>
            <span>Août 2022</span>
          </div>
    </div>
  )
}
