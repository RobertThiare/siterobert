import React from 'react';
import './Intro.css';
import profil from '../ressources/profil.png';
import Card from './Card';
import likemoji from '../ressources/likeEmoji.png';
import emoji from '../ressources/emoji.png';
import coeur from '../ressources/coeur.png';
import badEmo from '../ressources/boss.png';



export default function Intro() {
  return (
    <div className='intro'>
        <div className='i-left'>
            <div className='i-name'>
                <span>Salut! Je m'appelle</span>
                <span>Robert Thiare</span>
            </div>
            <div className='i-profil'>
                <img src={profil} alt="" />
            </div>
            <div className='title'>
                Développement mobile <br />
                Développeur web FullStack <br />
                Développement d'application de bureau
            </div>
        </div>
        <div className='i-right'>
            <div className='i-card1'>
                <Card
                emoji = {likemoji}
                heading = {'Conception d\'interface'}
                details = {"Photoshop, Gimp, Figma"} />
            </div>
            <div className='i-card2'>
                <Card
                emoji = {emoji}
                heading = {'Développement'}
                details = {"HTML5, CSS3, JAVASCRIPT, REACT, EXPRESS, C#, JAVA, ASP.NET CORE, SWIFT"} />
            </div>
            <div className='i-card3'>
                <Card
                emoji = {coeur}
                heading = {'Gestion de B.D'}
                details = {"MYSQL, SQLITE, SQL SERVER MONGODB, CASSANDRA"} />
            </div>
            <div className='i-card3'>
                <Card
                emoji = {badEmo}
                heading = {'Autres compétences'}
                details = {"GIT, DOCKER, AGILE, SCRUM, AZURE, ACTIVE DIRECTORY"} />
            </div>
        </div>
    </div>
  )
}
