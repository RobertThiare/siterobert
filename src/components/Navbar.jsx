import React, {useRef} from 'react';
import './Navbar.css';
import { Link } from 'react-scroll';
import Menu from '@iconscout/react-unicons/icons/uil-bars';
import Ferme from '@iconscout/react-unicons/icons/uil-times-square';

export default function Navbar() {
    const navRef = useRef();
    const navView = () => {
    navRef.current.classList.toggle("responsive_nav");
}

   
  return (
    <div className='n-wrapper' id='Navbar'>
        <div className='n-left'>
            <div className='n-name'>
                Portfolio
            </div>
        </div>
        <div className='n-right'>
            <nav className='n-nav' ref={navRef}>
                <ul style={{listStyleType: 'none'}}>
                    <Link spy={true} to='Navbar' smooth= {true} activeClass="activeClass" onClick={navView}>
                        <li>Accueil</li>
                    </Link>
                    <Link spy={true} to='Projets' smooth= {true} activeClass="activeClass" onClick={navView}>
                        <li>Projets</li>
                    </Link>
                    <Link spy={true} to='Contacts' smooth= {true} activeClass="activeClass" onClick={navView}>
                        <li>Contactez-moi</li>
                    </Link>
                    <button onClick={navView} className='nav-close'>
                        <Ferme size='1rem'/>
                    </button>
                
                </ul>
            </nav>
            <button onClick={navView} className='menu_button'>
                <Menu size='1.5rem'/>
            </button>
        </div>
       
    </div>
  )
}
