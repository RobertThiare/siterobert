import React from "react";
import './Page.css';

export default function Pages(props) {
    return <div>
                <nav className="p-link">
                    <ul>
                        <li>
                            <button onClick={props.changePage("patate")}> À la bonne patâte</button>
                        </li>
                        <li>
                            <button onClick={props.changePage("instachat")}>Insta-Chat</button>
                        </li>
                        <li>
                            <button onClick={props.changePage("quizz")}>Quizz en ligne</button>
                        </li>
                        <li>
                            <button onClick={props.changePage("stagiaire")}>Gestion stagiaires</button>
                        </li>
                    </ul>    
                </nav>  
            </div>
}