import React from 'react';
import AccPatate from '../ressources/Accueil.PNG';
import page1 from '../ressources/page5.PNG';
import page2 from '../ressources/page2.PNG';
import page3 from '../ressources/page3.PNG';
import page4 from '../ressources/page4.PNG';
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel';




export default function Projets() {
    const data = [
        {
            id: 1,
            image: AccPatate,
            alt: "restaurant en ligne version mobile",
            title: "Application de commande en ligne",
            technos: "Swift"
        },
        {
            id: 2,
            image: page2,
            alt: "Jeu de quiz pour web client",
            title: "Application Quizz Soccer",
            technos: "JavaScript, HTML5, CSS3"
        },
        {
            id: 3,
            image: page1,
            alt: "Logiciel de gestion des stagiaires",
            title: "Application de gestion de stagiaire",
            technos: "C# , WPF, XAML , SQL Server"
                    
        },
        {
            id: 4,
            image: page4,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
        {
            id: 5,
            image: page3,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        }
    ]
  return (
    <div className='projets' id='Projets'>
            <h1>Mes projets récents</h1>
            <Carousel>
                {
                    data.map(slide => (
                        <div key={slide.id}>
                            <img src={slide.image} alt={slide.title} />
                            <label>{slide.title}</label> <br />
                            <label>{slide.technos}</label> <br />
                            <a href={slide.link} download>Obtenir le code source</a>
                        </div>
                    ))
                }

            </Carousel>
        
        
    </div>

  )
}
