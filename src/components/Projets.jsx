import React from 'react';
import './Projets.css';
import Patate from './Patate';
import Quizz from './Quizz';
import Pages from './Pages';
import Stagiaire from './Stagiaire';
import Instachat from './instaChat';
import { useState } from 'react';

export default function Projets() {

    const [pageParDefaut, setPageParDefaut] = useState ('patate');
    const changePage = (projet) => {
        return () => {
            setPageParDefaut(projet)
        }
    }
    return (
    <div className='n-projects'>
                
                <div>
                {pageParDefaut === 'patate' &&
                    <Patate />
                }

                {pageParDefaut === 'instachat' &&
                    <Instachat />
                }
                {pageParDefaut === 'quizz' &&
                    <Quizz />
                }

                {pageParDefaut === 'stagiaire' &&
                    <Stagiaire />
                }
                </div>
                <Pages changePage={changePage} /> 
        </div>
    )
}