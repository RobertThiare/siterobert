import React from 'react';
import AccQuiz from '../ressources/P1Acc.PNG';
import question1 from '../ressources/P1Q1.PNG';
import question2 from '../ressources/P1Q2.PNG';
import question3 from '../ressources/P1Q3.PNG';
import question4 from '../ressources/P1Q4.PNG';
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel';




export default function Projets() {
    const data = [
   
        {
            id: 1,
            image: AccQuiz,
            alt: "Jeu de quiz pour web client",
            title: "Application Quizz Soccer",
            technos: "JavaScript, HTML5, CSS3"
        },
        {
            id: 2,
            image: question1,
            alt: "Logiciel de gestion des stagiaires",
            title: "Application de gestion de stagiaire",
            technos: "C# , WPF, XAML , SQL Server"
                    
        },
        {
            id: 3,
            image: question2,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
        {
            id: 4,
            image: question3,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
        {
            id: 5,
            image: question4,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
    ]
  return (
    <div className='projets' id='Projets'>
            <h1>Mes projets récents</h1>
            <Carousel>
                {
                    data.map(slide => (
                        <div key={slide.id}>
                            <img src={slide.image} alt={slide.title} />
                            <label>{slide.title}</label> <br />
                            <label>{slide.technos}</label> <br />
                            <a href={slide.link} download>Obtenir le code source</a>
                        </div>
                    ))
                }

            </Carousel>
        
        
    </div>

  )
}
