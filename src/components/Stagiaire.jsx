import React from 'react';
import stagiaire from '../ressources/Inter2.PNG';
import programme from '../ressources/Inter1.PNG';
import recherche from '../ressources/Inter3.PNG';
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel';




export default function Projets() {
    const data = [
   
        {
            id: 1,
            image: stagiaire,
            alt: "Jeu de quiz pour web client",
            title: "Application Quizz Soccer",
            technos: "JavaScript, HTML5, CSS3"
        },
        {
            id: 2,
            image: programme,
            alt: "Logiciel de gestion des stagiaires",
            title: "Application de gestion de stagiaire",
            technos: "C# , WPF, XAML , SQL Server"
                    
        },
        {
            id: 3,
            image: recherche,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
    ]
  return (
    <div className='projets' id='Projets'>
            <h1>Mes projets récents</h1>
            <Carousel>
                {
                    data.map(slide => (
                        <div key={slide.id}>
                            <img src={slide.image} alt={slide.title} />
                            <label>{slide.title}</label> <br />
                            <label>{slide.technos}</label> <br />
                            <a href={slide.link} download>Obtenir le code source</a>
                        </div>
                    ))
                }

            </Carousel>
        
        
    </div>

  )
}
