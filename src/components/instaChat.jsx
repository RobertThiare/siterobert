import React from 'react';
import AccChart from '../ressources/poste.PNG';
import compte from '../ressources/Compte.PNG';
import create from '../ressources/creationCompte.png';
import search from '../ressources/Recherche.PNG';
import user from '../ressources/user.PNG';
import './instaChat.css';
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel';




export default function Projets() {
    const data = [
   
        {
            id: 1,
            image: compte,
            alt: "Jeu de quiz pour web client",
            title: "Application Quizz Soccer",
            technos: "JavaScript, HTML5, CSS3"
        },
        {
            id: 2,
            image: create,
            alt: "Logiciel de gestion des stagiaires",
            title: "Application de gestion de stagiaire",
            technos: "C# , WPF, XAML , SQL Server"
                    
        },
        {
            id: 3,
            image: search,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
        {
            id: 4,
            image: AccChart,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
        {
            id: 5,
            image: user,
            alt: "application de chart en ligne",
            title: "Application de chart en ligne",
            technos: "Javascript, Nodejs, Expressjs"
        },
    ]
  return (
    <div className='projets' id='Projets'>
        <h1>Mes projets récents</h1>
            <Carousel>
                {
                    data.map(slide => (
                        <div key={slide.id}>
                            <img src={slide.image} alt={slide.title} />
                            <label>{slide.title}</label> <br />
                            <label>{slide.technos}</label> <br />
                            <a href={slide.link} download>Obtenir le code source</a>
                        </div>
                    ))
                }

            </Carousel>
        
        
    </div>

  )
}
